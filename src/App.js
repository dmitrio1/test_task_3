import React, { useState } from 'react';
import logo from './logo.svg';
import List from './views/components/List';
import './App.css';

import * as Names from './constants';


 /**
  * @TODO refactor, move utils to utils, make optimization, make styles, make tests
  */

const getRandomNumInRange = (start, end) => {
  return Math.floor(Math.random() * (end - start)) + start;
}

const pickRandomItemFromArray = (arr) => {
  return arr[getRandomNumInRange(0, arr.length)];
};


const makeBooks = (num, names) => {

  let books = [];

  for (let i = 0; i < num; i++) {
    const gender = pickRandomItemFromArray(names.genders);
    const namesList = gender === 'male' ? names.maleNames : names.femaleNames;

    books.push({
      name: pickRandomItemFromArray(names.bookNames),
      genre: pickRandomItemFromArray(names.genres),
      author: {
        name: pickRandomItemFromArray(namesList),
        gender,
      },
      publishDate: new Date(),
    })
  }

  return books;
}

const sortBy = (field) => {
  let order = 1

  return (data) => {
    let res = [...data];

    res.sort((a, b) => {
      if (getByPath(a, field) < getByPath(b, field)) {
        return order
      }
      else {
        return -order
      }
    });

    order = order * -1;

    return res;
  }

}

const filterBy = field => str => data => data.filter(item => {

  return getByPath(item, field).toLowerCase().includes(str.toLowerCase())

})

const filterByGenre = filterBy('genre');
const filterByGender = filterBy('author.gender');

function throttle(f, t) {
  let timer;
  return function (args) {
    if(timer){
      clearTimeout(timer);
    }

    timer = setTimeout(() => f(args), t)
  }
}

const getByPath = (obj, path) => {

  let paths = path.split('.');
  let current = obj;


  for (let i = 0; i < paths.length; i++) {
    if (current[paths[i]] == undefined) {
      return undefined;
    } else {
      current = current[paths[i]];
    }
  }
  return current;

}

const sortByBook = sortBy('name');
const sortByAuthor = sortBy('author.name');

const DAYS_IN_WEEK = 7;
const MAX_DAYS_IN_MONTH = 31;
const isHalloween = (date) => date.getDate() === 31 && date.getMonth() === 10
const isLastFriday = (date) => date.getDay() === 5 && date.getDate() + DAYS_IN_WEEK >= MAX_DAYS_IN_MONTH;

const ListItem = ({ book }) => {
  const pDate = new Date(book.publishDate);

  const isHighlightedHalloween = book.genre === 'horror' && isHalloween(pDate);
  const isHighlightedFriday = book.genre === 'financial' && isLastFriday(pDate);

  let clsName = 'listItem';
  if (isHighlightedFriday) clsName += ' highlightFriday';
  if (isHighlightedHalloween) clsName += ' hightHalloween';

  return <p className={clsName}>
    {
      [book.name, book.author.name, book.genre, book.publishDate.toLocaleDateString()].map(item => <span>{item} </span>)
    }
  </p>
}
const booksInit = makeBooks(1000000, Names);




function App() {
  const [books, setBooks] = useState(booksInit);
  const [filters, setFilters] = useState({
    genre: '',
    gender: ''
  });

  
  const screenHeight = window.innerHeight;
  const filteredByGenderBooks = filterByGender(filters.gender)(books);
  const filteredByGenreAndGenderBooks = filterByGenre(filters.genre)(filteredByGenderBooks);
  const setGenreFilter = (val) => setFilters({ ...filters, 'genre': val });
  const setGenderFilter = (val) => setFilters({ ...filters, 'gender': val });

  const throttledSetGenreFilter =  throttle(setGenreFilter, 1000);
  const throttledSetGenderFilter =  throttle(setGenderFilter, 1000);
  return (
    <div className="App">
      <input
        onChange={e => throttledSetGenreFilter(e.target.value)}
        placeholder='filter by genre'
      />
      <input
        onChange={(e) => throttledSetGenderFilter(e.target.value)}
        placeholder='filter by authors gender'
      />

      <p className='listItem'>
        <span title='sorting may take a long' style={{ cursor: 'pointer'}} onClick={() => setBooks(sortByBook(books))}>Name (clickable)</span>
        <span title='sorting may take a long' style={{ cursor: 'pointer'}} onClick={() => setBooks(sortByAuthor(books))}>Author (clickable)</span>
        <span>Genre</span>
        <span>Published</span>
      </p>

      <List
        itemData={filteredByGenreAndGenderBooks}
        height={screenHeight}
        itemHeight={30}
        itemComponent={ListItem}
      />
    </div>
  );
}

export default App;
