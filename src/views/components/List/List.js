import React, { useState } from 'react'
import PropTypes from 'prop-types'

import './list.scss';


const List = props => {
    const { height, width, itemData, itemHeight } = props;

    const [scrollState, setScrollState] = useState({
        scrollOffset: 0,
    });

    const viewPortStyles = {
        height: height + 'px',
        width: width ? width + 'px' : '100%'
    };

    const listStyles = {
        // it seems like browser has height limitation at 33554400px max
        height: (itemHeight * itemData.length) + 'px',
       
    }

    console.log(listStyles.height);

    const getFromIndex = (offset, itemHeight) => {
        return Math.floor(offset / itemHeight);
    }

    const getToIndex = (viewPortHeight, itemHeight, fromIndex) => {

        const visibleItemsCount = Math.ceil(viewPortHeight / itemHeight);

        return Math.max(0, fromIndex + visibleItemsCount);
    }

    const getFromToIndex = (
        {
            itemCount,
            itemHeight,
            viewPortHeight,
            scrollOffset,
        }) => {

        if (itemCount <= 0) {
            return [0, 0];
        }

        const fromIndex = getFromIndex(
            scrollOffset,
            itemHeight
        );

        const toIndex = getToIndex(
            viewPortHeight,
            itemHeight,
            fromIndex
        );

        return [
            fromIndex,
            toIndex
        ];
    }

    const getItemStyle = (index) => {
        const offset = itemHeight * index;

        return {
            top: offset + 'px',
            height: itemHeight + 'px',
            width: '100%'
        }
    }

    const onScroll = (event) => {

        const { scrollTop } = event.currentTarget;

        const scrollOffset = Math.max(
            0,
            Math.min(itemHeight * itemData.length, scrollTop)
        );

        setScrollState({
            scrollOffset,
        });
    };

    const getItems = () => {
        const [fromIndex, toIndex] = getFromToIndex(
            {
                itemCount: itemData.length,
                itemHeight,
                viewPortHeight: height,
                scrollOffset: scrollState.scrollOffset,
            }
        );



        const items = [];
        for (let index = fromIndex; index <= toIndex; index++) {
            if (itemData[index]) {
                items.push(
                    <li key={index} style={getItemStyle(index)} className='list__item'>
                        { props.itemComponent ? <props.itemComponent book={itemData[index]}/> : index }
                    </li>
                );
            }
        }

        return items;

    }

    return (
        <div className="list">
            <div
                className="list__viewport"
                style={viewPortStyles}
                onScroll={onScroll}
            >
                <ul style={listStyles}>
                    {
                        getItems()
                    }
                </ul>
            </div>
        </div>
    )
}

List.propTypes = {

}

export default List
